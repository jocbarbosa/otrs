const ViewSLA = require('../models/VIEW_SLA');
const js2xmlparser = require('js2xmlparser');
const Sequelize = require('sequelize');

const { Op } = Sequelize;

async function getAllTickets(req, res) {

    try {
        const countTickets = await ViewSLA.count();

        if (countTickets > 0) {

            const { TN, TITLE, QUEUE, TYPE, SERVICE, OWNER, CUSTOMER_ID, CUSTOMER_USER_ID, CREATE_BY, ESTADO_DO_SLA, create_time, initial_date, final_date } = req.query;

            let query = {};
            if (typeof TN != 'undefined') query.TN = TN;
            if (typeof TYPE != 'undefined') query.TYPE = TYPE;
            if (typeof SERVICE != 'undefined') query.SERVICE = SERVICE;
            if (typeof OWNER != 'undefined') query.OWNER = OWNER;
            if (typeof CUSTOMER_ID != 'undefined') query.CUSTOMER_ID = CUSTOMER_ID;
            if (typeof CUSTOMER_USER_ID != 'undefined') query.CUSTOMER_USER_ID = CUSTOMER_USER_ID;
            if (typeof CREATE_BY != 'undefined') query.CREATE_BY = CREATE_BY;
            if (typeof ESTADO_DO_SLA != 'undefined') query.ESTADO_DO_SLA = ESTADO_DO_SLA;

            if (typeof QUEUE != 'undefined') query.QUEUE = {
                [Op.like]: `%${QUEUE}%`,
            };

            if (typeof TITLE != 'undefined') query.TITLE = {
                [Op.like]: `%${TITLE}%`,
            };

            if (typeof req.query.initial_date != 'undefined' && typeof req.query.final_date == 'undefined') {
                query.create_time = {
                    [Op.gte]: Date.parse(initial_date)
                }
            } else if (typeof req.query.initial_date != 'undefined' && typeof req.query.final_date != 'undefined') {
                query.create_time = {
                    [Op.gte]: Date.parse(initial_date),
                    [Op.lt]: Date.parse(final_date),
                }
            } else if (typeof req.query.initial_date == 'undefined' && typeof req.query.final_date != 'undefined') {
                query.create_time = {
                    [Op.lt]: Date.parse(final_date)
                }
            }

            const limit = req.query.limit != null ? Number.parseInt(req.query.limit) : countTickets;
            const page = req.query.page != null ? (Number.parseInt(req.query.page) * limit) - limit : 0;


            try {
                const tickets = await ViewSLA.findAll({
                    raw: true,
                    offset: page,
                    limit: limit,
                    where: query
                });

                const ticketsResponse = {
                    total: countTickets,
                    tickets_found: tickets.length,
                    tickets
                }

                if (req.query.format != null && req.query.format == 'xml') {
                    const xmlConvert = js2xmlparser.parse('ticket', ticketsResponse);

                    res.set('Content-Type', 'application/xml');

                    return res.send(xmlConvert);
                }

                return res.json(ticketsResponse);

            } catch (err) {
                return res.json(err);
            }
        }

        return res.status(404).json({ message: 'No ticket found' });
    } catch (error) {
        return res.json(error);
    }


}

module.exports = { getAllTickets };