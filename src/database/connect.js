const Sequelize = require('sequelize');
const databaseSettings = require('../config/database.js');

const connection = new Sequelize(databaseSettings);

// Import models
const ViewSLA = require('../models/VIEW_SLA.js');

// Initialize connections
ViewSLA.init(connection);

module.exports = connection;