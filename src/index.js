require('dotenv/config');

const express = require('express');
const router = require('./routes');

const app = express();

require('./database/connect.js');

app.use(express.urlencoded({
    extended: false
}));
app.use(router);


app.listen(process.env.PORT, () => {
    console.log(`🔥 Server Up on port ${process.env.PORT} `);
})