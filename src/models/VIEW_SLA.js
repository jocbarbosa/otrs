const Sequelize = require('sequelize');
const { Model, DataTypes } = Sequelize;

class ViewSLA extends Model {
    static init(connection) {
        super.init({
            TN: {
                type: DataTypes.STRING,
                primaryKey: true
            },
            TITLE: DataTypes.STRING,
            QUEUE: DataTypes.STRING,
            TYPE: DataTypes.STRING,
            SERVICE: DataTypes.STRING,
            OWNER: DataTypes.STRING,
            CUSTOMER_ID: DataTypes.STRING,
            CUSTOMER_USER_ID: DataTypes.STRING,
            CREATE_BY: DataTypes.INTEGER,
            create_time: DataTypes.DATE,
            ESTADO_DO_SLA: DataTypes.STRING
        }, {
            timestamps: false,
            sequelize: connection,
            tableName: 'VIEW_SLA'
        });
    }
}

module.exports = ViewSLA;